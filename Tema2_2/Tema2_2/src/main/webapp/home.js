angular.module("app", [])
    .controller("appCtrl", function ($scope, $http) {
        var path = "http://localhost:8080/app/";
        $scope.year = 0;
        $scope.price = 0;
        $scope.capacity = 0;
        $scope.sellPrice = 0;
        $scope.taxPrice = 0;

        $scope.getPrice = function () {
            $http.get(path + $scope.price + '/' + $scope.year)
                .then(function (response) {
                    $scope.sellPrice = response.data;
                });
        }

        $scope.getTax = function () {
            $http.get(path + $scope.capacity)
                .then(function (response) {
                    $scope.taxPrice = response.data;
                });
        }
    });