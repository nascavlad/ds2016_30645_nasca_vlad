<html>
<head>
    <link rel="stylesheet" type="text/css" href="../home.css">
</head>
<body>
<div ng-app="app" ng-controller="appCtrl">

    <div class="labels">
        <label name="Year">Year</label><br>
        <label name="Price">Price</label><br>
        <label name="Capacity">Capacity</label><br>
    </div>

    <div class="values">
        <input type="text" ng-model="year"/><br>
        <input type="text" ng-model="price"/><br>
        <input type="text" ng-model="capacity"/><br>
    </div>

    <button ng-click="getPrice()">Price</button>
    <button ng-click="getTax()">Tax</button><br>

    Sell price: {{sellPrice}}<br>
    Tax  price: {{taxPrice}}<br>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="../home.js"></script>
</body>
</html>
