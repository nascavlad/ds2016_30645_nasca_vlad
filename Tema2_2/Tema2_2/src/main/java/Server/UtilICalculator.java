package Server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import Server.ICalculator;

/**
 * Created by vnasca on 11/16/2016.
 */
public class UtilICalculator extends UnicastRemoteObject implements ICalculator {

    protected UtilICalculator() throws RemoteException {
        super();
    }

    public double computePrice(Car c) throws RemoteException{
        return c.getPrice()-(c.getPrice()/7)*(2015-c.getYear());
    }

    public double computeTax(Car c) {
        if (c.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if (c.getEngineCapacity() > 1601) sum = 18;
        if (c.getEngineCapacity() > 2001) sum = 72;
        if (c.getEngineCapacity() > 2601) sum = 144;
        if (c.getEngineCapacity() > 3001) sum = 290;
        return c.getEngineCapacity() / 200.0 * sum;
    }
}
