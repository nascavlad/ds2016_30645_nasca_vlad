package Server;

import java.io.Serializable;

/**
 * Created by vnasca on 11/16/2016.
 */
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    private int year;
    private int engineCapacity;
    private double price;

    public Car() {
    }

    public Car(int year, int engineCapacity, double price) {
        this.year = year;
        this.engineCapacity = engineCapacity;
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (year != car.year) return false;
        return engineCapacity == car.engineCapacity;

    }

    @Override
    public int hashCode() {
        int result = year;
        result = 31 * result + engineCapacity;
        return result;
    }

    @Override
    public String toString() {
        return "Car2{" +
                "year=" + year +
                ", engineCapacity=" + engineCapacity +
                '}';
    }
}
