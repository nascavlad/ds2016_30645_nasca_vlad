package Server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by vnasca on 11/16/2016.
 */
public interface ICalculator extends Remote {

    double computePrice(Car c) throws RemoteException;

    double computeTax(Car c) throws RemoteException;
}