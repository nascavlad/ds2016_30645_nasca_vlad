package Server;

import java.net.UnknownHostException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by vnasca on 11/16/2016.
 */
public class Server {
    private static final int port = 8888;
    private static Registry registry;

    public Server() throws RemoteException, UnknownHostException {
        registry = LocateRegistry.createRegistry( port );
        try {
            registry.bind(ICalculator.class.getSimpleName(), new UtilICalculator());
        } catch (AlreadyBoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws RemoteException, UnknownHostException {
        Server server = new Server();
    }
}
