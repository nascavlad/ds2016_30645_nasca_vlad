package Client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.rmi.NotBoundException;

/**
 * Created by vnasca on 11/16/2016.
 */
@SpringBootApplication
public class Client {
    public static void main(String[] args) throws IOException, NotBoundException {
        SpringApplication.run(Client.class, args);
    }
}
