package Client;

import Server.ICalculator;
import Server.Car;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by vnasca on 11/16/2016.
 */
@Controller
@CrossOrigin
@RequestMapping("/app")
public class ClientController {

    private static final String HOST = "localhost";
    private static final int PORT = 8888;
    private static Registry registry;

    public ClientController() throws UnknownHostException {
    }

    @RequestMapping( method = RequestMethod.GET)
    @ResponseBody
    double calculatePrice(HttpServletRequest request, HttpServletResponse response) throws RemoteException, NotBoundException {
        registry = LocateRegistry.getRegistry(HOST, PORT);
        ICalculator iPriceService = (ICalculator) registry.lookup(ICalculator.class.getSimpleName());
        return 3;
    }

    @RequestMapping(path = "/{price}/{year}", method = RequestMethod.GET)
    @ResponseBody
    public double retrieveUsers(@PathVariable("price") String price, @PathVariable("year") String year, HttpServletRequest request, HttpServletResponse response) throws RemoteException, NotBoundException {

        ICalculator ICalculator;
        registry = LocateRegistry.getRegistry(HOST,PORT);
        ICalculator = (ICalculator) registry.lookup(ICalculator.class.getSimpleName());

        Car c = new Car();
        c.setYear(Integer.parseInt(year));
        c.setPrice(Integer.parseInt(price));

        return ICalculator.computePrice(c);
    }

    @RequestMapping(path = "/{capacity}", method = RequestMethod.GET)
    @ResponseBody
    public double retrieveUsers(@PathVariable("capacity") String capacity, HttpServletRequest request, HttpServletResponse response) throws RemoteException, NotBoundException {

        ICalculator ICalculator;
        registry = LocateRegistry.getRegistry(HOST,PORT);
        ICalculator = (ICalculator) registry.lookup(ICalculator.class.getSimpleName());

        Car c = new Car();
        c.setEngineCapacity(Integer.parseInt(capacity));

        return ICalculator.computeTax(c);
    }

}
