package ds.tema4.service;

import com.querydsl.core.types.Predicate;
import ds.tema4.Repo.CustomerRepository;
import ds.tema4.Repo.PackageRepository;
import ds.tema4.Repo.RouteRepository;
import ds.tema4.model.*;
import ds.tema4.model.Package;
import ds.tema4.utils.Converter;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by vnasca on 1/3/2017.
 */
@Component
public class TrackingService {

    @Autowired
    private CustomerRepository customerRepo;

    @Autowired
    private PackageRepository packageRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private Converter converter;

    public Customer getCustomer(Long customerId){
        QCustomer qCustomer = QCustomer.customer;
        Predicate pred = qCustomer.id.eq(customerId);
        Customer customer = customerRepo.findOne(pred);
        return customer;
    }

    public PackagePojo getPackage(Long packageId){
        QPackage qPackage = QPackage.package$;
        Predicate pred = qPackage.id.eq(packageId);
        Package pack = packageRepository.findOne(pred);
        return converter.convert(pack);
    }

    public List<PackagePojo> getPackages(){
        return converter.convertPackages(packageRepository.findAll());
    }

    public PackagePojo createPackage(PackagePojo packagePojo){
        Package pack = converter.convert(packagePojo);
        return converter.convert(packageRepository.save(pack));
    }

    public PackagePojo delete(Long id){
        QRoute qRoute = QRoute.route;
        Predicate pred = qRoute.pack.id.eq(id);
        List<Route> routes = (List<Route>) routeRepository.findAll(pred);

        for(Route route: routes) {
            routeRepository.delete(route.getId());
        }

        PackagePojo pack = getPackage(id);
        packageRepository.delete(id);
        return pack;
    }

    public PackagePojo track(Long packageId, String srcCityId, String destCityId){
        QPackage qPackage = QPackage.package$;
        Predicate pred = qPackage.id.eq(packageId);
        Package pack = packageRepository.findOne(pred);

        pack.setSenderCity(srcCityId);
        pack.setDestinationCity(destCityId);
        pack.setTracking(true);
        packageRepository.save(pack);

        Route r1 = new Route(pack.getSenderCity(), "11.12.2016");
        r1.setPack(pack);
        Route r2 = new Route(pack.getDestinationCity(), "20.12.2016");
        r2.setPack(pack);
        routeRepository.save(r1);
        routeRepository.save(r2);
        return converter.convert(pack);
    }

    public RoutePojo createRoute(RoutePojo routePojo){
        Route route = converter.convert(routePojo);
        return converter.convert(routeRepository.save(route));
    }

    public List<RouteResponse> getAllRoutes(long id){
        QRoute qRoute = QRoute.route;
        Predicate pred = qRoute.pack.id.eq(id);
        List<Route> routes = (List<Route>) routeRepository.findAll(pred);
        return converter.convertRoutesToList(routes);
    }

    public void register(String username, String password) {
        Customer c = new Customer("user", username, password);
        customerRepo.save(c);
    }

    public LoginResponse login(String username){
        QCustomer qCustomer = QCustomer.customer;
        Predicate pred = qCustomer.username.eq(username);
        Customer c = customerRepo.findOne(pred);
        if(c == null){
            return null;
        }
        LoginResponse response = new LoginResponse();
        LoginPojo pojo = new LoginPojo();
        pojo.setId(c.getId());
        pojo.setPassword(c.getPassword());
        pojo.setUsername(c.getUsername());
        response.setCredentials(pojo);
        return response;
    }

    @PostConstruct
    public void init() {
        Customer c1 = new Customer("Nasca Vlad", "user", "user");
        Customer c2 = new Customer("Andrei Dan", "nascavlad2", "parola");
        customerRepo.save(c1);
        customerRepo.save(c2);

        Package pck = new Package();
        pck.setSender(c1);
        pck.setReciever(c2);
        pck.setName("package 1");
        pck.setDescription("description");
        pck.setDestinationCity("Cluj");
        pck.setSenderCity("Bucharest");
        pck.setStatus("shipping");
        packageRepository.save(pck);

        Package pck2 = new Package();
        pck2.setSender(c1);
        pck2.setReciever(c2);
        pck2.setName("package 2");
        pck2.setDescription("description");
        pck2.setDestinationCity("Cluj");
        pck2.setSenderCity("Bucharest");
        pck2.setStatus("shipping");
        packageRepository.save(pck2);

    }


}
