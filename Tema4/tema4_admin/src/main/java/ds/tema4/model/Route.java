package ds.tema4.model;

import javax.persistence.*;

/**
 * Created by vnasca on 1/7/2017.
 */
@Entity
@Table(name = "T_ROUTE")
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PACKAGE_ID")
    private Package pack;

    @Column(name = "CITY")
    private String city;

    @Column(name = "DATE")
    private String date;

    public Route(){

    }

    public Route(String city, String date) {
        this.city = city;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Package getPack() {
        return pack;
    }

    public void setPack(Package pack) {
        this.pack = pack;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
