package ds.tema4.controller;

/**
 * Created by vnasca on 1/7/2017.
 */

import ds.tema4.service.TrackingService;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class DsEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    @Autowired
    private TrackingService service;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPackages")
    @ResponsePayload
    public PackageList getAllPackages(@RequestPayload GetAllPackages request) {
        PackageList list = new PackageList();
        List<PackageResponse> pkgList = new ArrayList();
        List<PackagePojo> response = service.getPackages();
        for(PackagePojo pack: response){
            PackageResponse pResp = new PackageResponse();
            pResp.setPackagePojo(pack);
            pkgList.add(pResp);
        }

        list.getPackageResponse().addAll(pkgList);
        return list;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPackageByIdRequest")
    @ResponsePayload
    public PackageResponse getPackageById(@RequestPayload GetPackageByIdRequest request) {
        PackageResponse response = new PackageResponse();
        response.setPackagePojo(service.getPackage(request.getId()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createPackageRequest")
    @ResponsePayload
    public PackageResponse createPackage(@RequestPayload CreatePackageRequest request) {
        PackageResponse response = new PackageResponse();
        response.setPackagePojo(service.createPackage(request.getPackagePojo()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deletePackageRequest")
    @ResponsePayload
    public PackageResponse deletePackage(@RequestPayload DeletePackageRequest request) {
        PackageResponse response = new PackageResponse();
        response.setPackagePojo(service.delete(request.getId()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createRouteRequest")
    @ResponsePayload
    public RouteResponse createRoute(@RequestPayload CreateRouteRequest request) {
        RouteResponse response = new RouteResponse();
        response.setRoutePojo(service.createRoute(request.getRoutePojo()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRoutes")
    @ResponsePayload
    public RouteList getAllRoutes(@RequestPayload GetAllRoutes request) {
        RouteList response = new RouteList();
        response.getRouteResponse().addAll(service.getAllRoutes(request.getPackageId()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "trackRequest")
    @ResponsePayload
    public PackageResponse track(@RequestPayload TrackRequest request) {
        PackageResponse response = new PackageResponse();
        response.setPackagePojo(service.track(request.getTrackPojo().getPackageId(), request.getTrackPojo().getSrc(), request.getTrackPojo().getDest()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "registerRequest")
    @ResponsePayload
    public void track(@RequestPayload RegisterRequest request) {
        service.register(request.getUsername(), request.getPassword());
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "loginRequest")
    @ResponsePayload
    public LoginResponse track(@RequestPayload LoginRequest request) {
        return service.login(request.getUsername());
    }

}