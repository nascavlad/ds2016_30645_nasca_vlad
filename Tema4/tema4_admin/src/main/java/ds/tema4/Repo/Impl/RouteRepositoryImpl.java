package ds.tema4.Repo.Impl;

import ds.tema4.Repo.PackageRepository;
import ds.tema4.Repo.RouteRepository;
import ds.tema4.model.Package;
import ds.tema4.model.Route;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

/**
 * Created by vnasca on 1/7/2017.
 */
@Repository
@Transactional
public class RouteRepositoryImpl extends QueryDslJpaRepository<Route, Long> implements RouteRepository {
    @SuppressWarnings("unchecked")
    public RouteRepositoryImpl(EntityManager em) {
        super((JpaEntityInformationSupport<Route, Long>) JpaEntityInformationSupport
                .getEntityInformation(Route.class, em), em);
    }
}
