package ds.tema4.Repo;

import ds.tema4.model.Customer;
import ds.tema4.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by vnasca on 1/7/2017.
 */
@Component
@Repository
@Transactional
public interface RouteRepository extends JpaRepository<Route, Long>, QueryDslPredicateExecutor<Route> {

}