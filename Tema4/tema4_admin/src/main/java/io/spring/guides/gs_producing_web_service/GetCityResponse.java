//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.01.07 at 09:09:27 PM EET 
//


package io.spring.guides.gs_producing_web_service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cityPojo" type="{http://spring.io/guides/gs-producing-web-service}cityPojo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cityPojo"
})
@XmlRootElement(name = "getCityResponse")
public class GetCityResponse {

    @XmlElement(required = true)
    protected CityPojo cityPojo;

    /**
     * Gets the value of the cityPojo property.
     * 
     * @return
     *     possible object is
     *     {@link CityPojo }
     *     
     */
    public CityPojo getCityPojo() {
        return cityPojo;
    }

    /**
     * Sets the value of the cityPojo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CityPojo }
     *     
     */
    public void setCityPojo(CityPojo value) {
        this.cityPojo = value;
    }

}
