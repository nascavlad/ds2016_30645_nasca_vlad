package ds.tema4.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPackage is a Querydsl query type for Package
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPackage extends EntityPathBase<Package> {

    private static final long serialVersionUID = 308362211L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPackage package$ = new QPackage("package$");

    public final StringPath description = createString("description");

    public final StringPath destinationCity = createString("destinationCity");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final QCustomer reciever;

    public final QCustomer sender;

    public final StringPath senderCity = createString("senderCity");

    public final StringPath status = createString("status");

    public final BooleanPath tracking = createBoolean("tracking");

    public QPackage(String variable) {
        this(Package.class, forVariable(variable), INITS);
    }

    public QPackage(Path<? extends Package> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPackage(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPackage(PathMetadata metadata, PathInits inits) {
        this(Package.class, metadata, inits);
    }

    public QPackage(Class<? extends Package> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.reciever = inits.isInitialized("reciever") ? new QCustomer(forProperty("reciever")) : null;
        this.sender = inits.isInitialized("sender") ? new QCustomer(forProperty("sender")) : null;
    }

}

