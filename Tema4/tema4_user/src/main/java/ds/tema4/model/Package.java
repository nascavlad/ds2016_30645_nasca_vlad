package ds.tema4.model;

import javax.persistence.*;

/**
 * Created by vnasca on 1/3/2017.
 */
@Entity
@Table(name = "T_PACKAGE")
public class Package {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "SENDER_CUSTOMER_ID")
    private Customer sender;

    @ManyToOne
    @JoinColumn(name = "RECEIVER_CUSTOMER_ID")
    private Customer reciever;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "SENDER_CITY_ID")
    private String senderCity;

    @Column(name = "DEST_CITY_ID")
    private String destinationCity;

    @Column(name = "TRACKING")
    private boolean tracking = false;

    @Column(name = "STATUS")
    private String status;

    public Package(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getSender() {
        return sender;
    }

    public void setSender(Customer sender) {
        this.sender = sender;
    }

    public Customer getReciever() {
        return reciever;
    }

    public void setReciever(Customer reciever) {
        this.reciever = reciever;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
