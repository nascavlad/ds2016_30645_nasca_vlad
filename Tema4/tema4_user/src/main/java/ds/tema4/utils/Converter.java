package ds.tema4.utils;

import ds.tema4.model.Package;
import ds.tema4.model.Route;
import ds.tema4.service.TrackingService;
import io.spring.guides.gs_producing_web_service.PackagePojo;
import io.spring.guides.gs_producing_web_service.RoutePojo;
import io.spring.guides.gs_producing_web_service.RouteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vnasca on 1/7/2017.
 */
@Component
public class Converter {

    @Autowired
    private TrackingService service;

    public PackagePojo convert(Package pack) {
        PackagePojo pojo = new PackagePojo();
        pojo.setId(pack.getId());
        pojo.setSender(pack.getSender().getId());
        pojo.setReceiver(pack.getReciever().getId());
        pojo.setName(pack.getName());
        pojo.setDescription(pack.getDescription());
        pojo.setSenderCity(pack.getSenderCity());
        pojo.setDestinationCity(pack.getDestinationCity());
        pojo.setTracking(pack.isTracking());
        pojo.setStatus(pack.getStatus());

        return pojo;
    }

    public Package convert(PackagePojo pojo) {
        Package pack = new Package();
        pack.setId(pojo.getId());
        pack.setSender(service.getCustomer(pojo.getSender()));
        pack.setReciever(service.getCustomer(pojo.getReceiver()));
        pack.setName(pojo.getName());
        pack.setDescription(pojo.getDescription());
        pack.setSenderCity(pojo.getSenderCity());
        pack.setDestinationCity(pojo.getDestinationCity());
        pack.setTracking(pojo.isTracking());
        pack.setStatus(pojo.getStatus());

        return pack;
    }

    public RoutePojo convert(Route route) {
        RoutePojo pojo = new RoutePojo();
        pojo.setId(route.getId());
        pojo.setPackageId(route.getPack().getId());
        pojo.setCity(route.getCity());
        pojo.setDate(route.getDate());

        return pojo;
    }

    public Route convert(RoutePojo pojo) {
        Route route = new Route();
        route.setId(pojo.getId());
        route.setPack(convert(service.getPackage(pojo.getPackageId())));
        route.setCity(pojo.getCity());
        route.setDate(pojo.getDate());

        return route;
    }

    public List<RoutePojo> convertRoutes (List<Route> listRoute) {
        List<RoutePojo> pojos = new ArrayList<RoutePojo>();
        for(Route route: listRoute){
            pojos.add(convert(route));
        }

        return pojos;
    }

    public List<RouteResponse> convertRoutesToList (List<Route> listRoute) {
        List<RouteResponse> routes = new ArrayList<RouteResponse>();
        for(Route route: listRoute){
            RouteResponse rs = new RouteResponse();
            rs.setRoutePojo(convert(route));
            routes.add(rs);
        }
        return routes;
    }

    public List<PackagePojo> convertPackages (List<Package> listPackages) {
        List<PackagePojo> pojos = new ArrayList<PackagePojo>();
        for(Package pack: listPackages){
            pojos.add(convert(pack));
        }

        return pojos;
    }
}

