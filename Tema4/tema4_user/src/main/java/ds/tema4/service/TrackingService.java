package ds.tema4.service;

import com.querydsl.core.types.Predicate;
import ds.tema4.Repo.CustomerRepository;
import ds.tema4.Repo.PackageRepository;
import ds.tema4.Repo.RouteRepository;
import ds.tema4.model.*;
import ds.tema4.model.Package;
import ds.tema4.utils.Converter;
import io.spring.guides.gs_producing_web_service.PackagePojo;
import io.spring.guides.gs_producing_web_service.RouteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vnasca on 1/3/2017.
 */
@Component
public class TrackingService {

    private long clientId = 1L;

    @Autowired
    private CustomerRepository customerRepo;

    @Autowired
    private PackageRepository packageRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private Converter converter;

    public Customer getCustomer(Long customerId) {
        QCustomer qCustomer = QCustomer.customer;
        Predicate pred = qCustomer.id.eq(customerId);
        Customer customer = customerRepo.findOne(pred);
        return customer;
    }

    public PackagePojo getPackage(Long packageId) {
        QPackage qPackage = QPackage.package$;
        Predicate predSender = qPackage.id.eq(packageId).and(qPackage.sender.id.eq(clientId));
        Predicate predReciever = qPackage.id.eq(packageId).and(qPackage.reciever.id.eq(clientId));
        Package sent = packageRepository.findOne(predSender);
        Package toRecieve = packageRepository.findOne(predReciever);
        if (sent != null) {
            return converter.convert(sent);
        } else if (toRecieve != null) {
            return converter.convert(toRecieve);
        } else {
            return null;
        }
    }

    public List<PackagePojo> getPackages() {
        QPackage qPackage = QPackage.package$;
        Predicate predSender = qPackage.sender.id.eq(clientId);
        Predicate predReciever = qPackage.reciever.id.eq(clientId);
        List<Package> sent = (List<Package>) packageRepository.findAll(predSender);
        List<Package> toRecieve = (List<Package>) packageRepository.findAll(predReciever);

        List<Package> all = new ArrayList<Package>();
        if (sent != null) {
            all.addAll(sent);
        }
        if (toRecieve != null) {
            all.addAll(toRecieve);
        }

        if (all.size() > 0) {

            all = removeDuplicates(all);
            return converter.convertPackages(all);
        } else {
            return null;
        }
    }

    public List<PackagePojo> getPackagesByTracking(boolean tracking) {
        QPackage qPackage = QPackage.package$;
        Predicate predSender;
        Predicate predReciever;

        if (tracking) {
            predSender = qPackage.sender.id.eq(clientId).and(qPackage.tracking.isTrue());
            predReciever = qPackage.reciever.id.eq(clientId).and(qPackage.tracking.isTrue());
        } else {
            predSender = qPackage.sender.id.eq(clientId).and(qPackage.tracking.isFalse());
            predReciever = qPackage.reciever.id.eq(clientId).and(qPackage.tracking.isFalse());
        }

        List<Package> sent = (List<Package>) packageRepository.findAll(predSender);
        List<Package> toRecieve = (List<Package>) packageRepository.findAll(predReciever);

        List<Package> all = new ArrayList<Package>();
        if (sent != null) {
            all.addAll(sent);
        }
        if (toRecieve != null) {
            all.addAll(toRecieve);
        }

        if (all.size() > 0) {
            all = removeDuplicates(all);
            return converter.convertPackages(all);
        } else {
            return null;
        }
    }

    public String getPackageStatus(long packageId) {
        PackagePojo pack = getPackage(packageId);
        return pack.getStatus();
    }

    public List<RouteResponse> getAllRoutes(long id) {
        QRoute qRoute = QRoute.route;
        Predicate pred = qRoute.pack.id.eq(id);
        List<Route> routes = (List<Route>) routeRepository.findAll(pred);
        return converter.convertRoutesToList(routes);
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    private List<Package> removeDuplicates(List<Package> all) {
        List<Package> toRemove = new ArrayList<Package>();
        for(int i=0;i<all.size()-1;i++){
            for(int j = 0; j<all.size(); j++){
                if(all.get(i).getId() == all.get(j).getId()){
                    toRemove.add(all.get(j));
                }
            }
        }
        all.removeAll(toRemove);
        return all;
    }
}
