package ds.tema4.controller;

/**
 * Created by vnasca on 1/7/2017.
 */

import ds.tema4.service.TrackingService;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class DsEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    @Autowired
    private TrackingService service;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPackages")
    @ResponsePayload
    public PackageList getAllPackages(@RequestPayload GetAllPackages request) {
        PackageList list = new PackageList();
        List<PackageResponse> pkgList = new ArrayList();
        List<PackagePojo> response = service.getPackages();
        for(PackagePojo pack: response){
            PackageResponse pResp = new PackageResponse();
            pResp.setPackagePojo(pack);
            pkgList.add(pResp);
        }

        list.getPackageResponse().addAll(pkgList);
        return list;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPackageByTracking")
    @ResponsePayload
    public PackageList getPackageByTracking(@RequestPayload GetPackageByTracking request) {
        PackageList list = new PackageList();
        List<PackageResponse> pkgList = new ArrayList();
        List<PackagePojo> response = service.getPackagesByTracking(request.isTracking());
        for(PackagePojo pack: response){
            PackageResponse pResp = new PackageResponse();
            pResp.setPackagePojo(pack);
            pkgList.add(pResp);
        }

        list.getPackageResponse().addAll(pkgList);
        return list;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPackageByIdRequest")
    @ResponsePayload
    public PackageResponse getPackageById(@RequestPayload GetPackageByIdRequest request) {
        PackageResponse response = new PackageResponse();
        response.setPackagePojo(service.getPackage(request.getId()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPackageStatus")
    @ResponsePayload
    public StatusResponse getPackageStatus(@RequestPayload GetPackageStatus request) {
        StatusResponse resp = new StatusResponse();
        StatusPojo response = new StatusPojo();
        response.setStatus(service.getPackageStatus(request.getPackageId()));
        resp.setStatusPojo(response);
        return resp;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRoutes")
    @ResponsePayload
    public RouteList getAllRoutes(@RequestPayload GetAllRoutes request) {
        RouteList response = new RouteList();
        response.getRouteResponse().addAll(service.getAllRoutes(request.getPackageId()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "setClientId")
    @ResponsePayload
    public void setClientId(@RequestPayload SetClientId request) {
        service.setClientId(request.getId());
    }

}