package ds.tema4.Repo.Impl;

import ds.tema4.Repo.CustomerRepository;
import ds.tema4.model.Customer;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

/**
 * Created by vnasca on 1/3/2017.
 */
@Repository
@Transactional
public class CustomerRepositoryImpl extends QueryDslJpaRepository<Customer, Long> implements CustomerRepository {
    @SuppressWarnings("unchecked")
    public CustomerRepositoryImpl(EntityManager em) {
        super((JpaEntityInformationSupport<Customer, Long>) JpaEntityInformationSupport
                .getEntityInformation(Customer.class, em), em);
    }
}