package ds.tema4.Repo.Impl;

import ds.tema4.model.Package;
import ds.tema4.Repo.PackageRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

/**
 * Created by vnasca on 1/3/2017.
 */
@Repository
@Transactional
public class PackageRepositoryImpl extends QueryDslJpaRepository<Package, Long> implements PackageRepository {
    @SuppressWarnings("unchecked")
    public PackageRepositoryImpl(EntityManager em) {
        super((JpaEntityInformationSupport<Package, Long>) JpaEntityInformationSupport
                .getEntityInformation(Package.class, em), em);
    }
}
