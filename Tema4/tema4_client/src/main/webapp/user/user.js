angular.module("app", [])
    .controller("appCtrl", function ($scope, $http) {

        $scope.packs = {};
        $scope.packsRoutes = [];
        $scope.trackStatus = true;
        $scope.responsePackages = null;
        $scope.listOfPacks = "";
        $scope.idStatus = null;
        $scope.status = null;
        init();

        function init() {
            getPackages();
        }

        function getPackages() {
            $http.get('/user/getAllPackages')
                .then(function (response) {
                    $scope.packs = response.data;
                    createRoutes();
                });
        }

        function createRoutes() {
            var i = 0;
            var l = $scope.packs.length;
            for (i; i < l; i++) {
                $scope.packsRoutes.push([]);
                setRoutes($scope.packs[i].id, i);
            }
        }

        function setRoutes(id, indice) {
            var url = '/user/getAllRoutes/' + id;
            $http.get(url)
                .then(function (response) {
                    $scope.packsRoutes[indice] = response.data;
                    $scope.packs[indice].routes = response.data;
                });
        }

        $scope.trackPack = function () {
            var url = '/user/getPackageByTracking/' + $scope.trackStatus;
            $scope.listOfPacks = "";
            $http.get(url)
                .then(function (response) {
                    $scope.responsePackages = response.data;


                    var i = 0;
                    var l = $scope.responsePackages.length;
                    for (i; i < l; i++) {
                        $scope.listOfPacks = $scope.listOfPacks + " " + $scope.responsePackages[i].id;
                    }
                });
        }

        $scope.getStatus = function () {
            var url = '/user/getPackageStatus/' + $scope.idStatus;
            $http.get(url)
                .then(function (response) {
                    $scope.status = response.data.status;
                });
        }

    });