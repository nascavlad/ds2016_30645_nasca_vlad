angular.module("app", [])
    .controller("appCtrl", function ($scope, $http) {

        $scope.packs = {};
        $scope.packsRoutes = [];
        $scope.packToCreate = {};
        $scope.routeToCreate = {};
        $scope.trackPackage = {};
        init();

        function init() {
            getPackages();
        }

        function getPackages() {
            $http.get('/admin/getAllPackages')
                .then(function (response) {
                    $scope.packs = response.data;
                    createRoutes();
                });
        }

        function createRoutes() {
            var i = 0;
            var l = $scope.packs.length;
            for (i; i < l; i++) {
                $scope.packsRoutes.push([]);
                setRoutes($scope.packs[i].id, i);
            }
        }

        function setRoutes(id, indice) {
            var url = '/admin/getAllRoutes/' + id;
            $http.get(url)
                .then(function (response) {
                    $scope.packsRoutes[indice] = response.data;
                    $scope.packs[indice].routes = response.data;
                });
        }

        $scope.createPackage = function () {
            $scope.packToCreate.tracking = false;
            $scope.packToCreate.status = "shipping";
            var toCreate = [];
            toCreate.tracking = false;

            var headers = {'X-CSRF-TOKEN': $scope.csrf}
            $http.post('/admin/createPackage', $scope.packToCreate, {headers: headers})
                .then(function (response) {

                        init();
                        $scope.packToCreate = {};

                    },
                    function (response) {
                    }
                );
        }

        $scope.createRoute = function () {

            var toCreate = {};
            toCreate.city = $scope.routeToCreate.city;
            toCreate.date = $scope.routeToCreate.date;

            var headers = {'X-CSRF-TOKEN': $scope.csrf}
            $http.post('/admin/createRoute', $scope.routeToCreate, {headers: headers})
                .then(function (response) {
                        init();
                        $scope.routeToCreate = {};
                    },
                    function (response) {
                    }
                );
        }

        $scope.deletePackage = function (id) {
            debugger;
            $http.delete('/admin/deletePackage/' + id)
                .then(function (response) {
                        debugger;
                        init();
                    },
                    function (response) {
                    }
                );
        }

        $scope.trackPack = function () {

            var headers = {'X-CSRF-TOKEN': $scope.csrf}
            $http.put('/admin/track', $scope.trackPackage, {headers: headers})
                .then(function (response) {

                        init();
                        $scope.trackPackage = {};

                    },
                    function (response) {
                    }
                );
        }
    });