package hello.service;

import hello.wsdl.*;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;


public class ServiceAdmin extends WebServiceGatewaySupport {

    public PackageList getAllPackages() {
        GetAllPackages request = new GetAllPackages();
        request.setPackageId(0L);
        return (PackageList) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public PackageResponse getPackageById(long id) {
        GetPackageByIdRequest request = new GetPackageByIdRequest();
        request.setId(id);
        return (PackageResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public PackageResponse createPackage(PackagePojo pack) {
        CreatePackageRequest request = new CreatePackageRequest();
        request.setPackagePojo(pack);
        return (PackageResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public PackageResponse deletePackage(long id) {
        DeletePackageRequest request = new DeletePackageRequest();
        request.setId(id);
        return (PackageResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public RouteResponse createRoute(RoutePojo route) {
        CreateRouteRequest request = new CreateRouteRequest();
        request.setRoutePojo(route);
        return (RouteResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public RouteList getAllRoutes(long id) {
        GetAllRoutes request = new GetAllRoutes();
        request.setPackageId(id);
        return (RouteList) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public PackageResponse track(TrackPojo trackPojo) {
        TrackRequest request = new TrackRequest();
        request.setTrackPojo(trackPojo);
        return (PackageResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public LoginPojo login(String username){
        LoginRequest request = new LoginRequest();
        request.setUsername(username);
        LoginResponse resp = (LoginResponse) getWebServiceTemplate().marshalSendAndReceive(request);

        if(resp == null) {
            return null;
        }

        return resp.getCredentials();
    }

    public void register(String username, String password){
        RegisterRequest request = new RegisterRequest();
        request.setUsername(username);
        request.setPassword(password);
        getWebServiceTemplate().marshalSendAndReceive(request);
    }
}