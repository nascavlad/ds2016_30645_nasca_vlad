package hello.service;

import hello.wsdl.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 * Created by vnasca on 1/12/2017.
 */
public class ServiceUser extends WebServiceGatewaySupport {

    private long clientId;

    public PackageList getAllPackages() {
        GetAllPackages request = new GetAllPackages();
        request.setPackageId(0L);
        return (PackageList) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public PackageResponse getPackageById(long id) {
        GetPackageByIdRequest request = new GetPackageByIdRequest();
        request.setId(id);
        return (PackageResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public PackageList getPackageByTracking(boolean tracking){
        GetPackageByTracking request = new GetPackageByTracking();
        request.setTracking(tracking);
        return (PackageList) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public RouteList getAllRoutes(long id) {
        GetAllRoutes request = new GetAllRoutes();
        request.setPackageId(id);
        return (RouteList) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    public StatusPojo getPackageStatus(long id){
        GetPackageStatus request = new GetPackageStatus();
        request.setPackageId(id);
        return ((StatusResponse) getWebServiceTemplate().marshalSendAndReceive(request)).getStatusPojo();
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        SetClientId request = new SetClientId();
        request.setId(clientId);
        this.clientId = clientId;
        getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
