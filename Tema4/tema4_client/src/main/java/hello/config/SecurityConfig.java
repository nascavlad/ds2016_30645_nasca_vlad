package hello.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * Created by vnasca on 1/16/2017.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationSuccessHandler loginSuccessHandler;

//    @Autowired
//    private AuthenticationSuccessHandler loginSuccessHandler;

    @Override
    public void configure(HttpSecurity http) throws Exception {

        //http;
        http//.addFilterBefore(new RequestFilter(), RequestFilter.class)
            .csrf().disable()
            .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/login").permitAll()
            .antMatchers("/", "/reg", "/register.html", "/register.js", "register.js").permitAll()
            //.antMatchers("/admin/**").hasAuthority("ADMIN")
            //.antMatchers("/user/**").hasAuthority("USER")
           // .anyRequest().authenticated()
            .and()
            .authorizeRequests()
            .antMatchers("/login").permitAll()
            .antMatchers("/register").permitAll()
            //.anyRequest().authenticated()
            .and()
            .formLogin()
//                .loginPage("/")
            .successHandler(loginSuccessHandler)
            .usernameParameter("username")
            .passwordParameter("password");

    }

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth)
//            throws Exception {
//        auth.inMemoryAuthentication().withUser("admin")
//                .password("admin").roles("ADMIN");
//    }
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }
}