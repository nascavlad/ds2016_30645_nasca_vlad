package hello.config;

import hello.service.ServiceAdmin;
import hello.service.ServiceUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * Created by vnasca on 1/11/2017.
 */

@Configuration
public class SoapClientConfig {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("hello.wsdl");
        return marshaller;
    }

    @Bean
    public ServiceAdmin packagePojoClient(Jaxb2Marshaller marshaller) {
        ServiceAdmin client = new ServiceAdmin();
        client.setDefaultUri("http://localhost:8080/ws/countries");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

    @Bean
    public ServiceUser packagePojoClient2(Jaxb2Marshaller marshaller) {
        ServiceUser client = new ServiceUser();
        client.setDefaultUri("http://localhost:8081/ws/countries");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}