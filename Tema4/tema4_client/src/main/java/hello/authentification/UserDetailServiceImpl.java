package hello.authentification;

import hello.Application;
import hello.service.ServiceAdmin;
import hello.service.ServiceUser;
import hello.wsdl.LoginPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by vnasca on 1/16/2017.
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Application.class.getName() + "." + UserDetailServiceImpl.class.getName());

    @Autowired
    private ServiceAdmin service;

    @Autowired
    private ServiceUser serviceUser;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {

        if (username.equals("admin")) {
            Set<GrantedAuthority> auths = new HashSet<GrantedAuthority>();
            GrantedAuthority auth = new SimpleGrantedAuthority("ADMIN");
            auths.add(auth);
            return new User("admin", "admin", auths);
        } else {
            LoginPojo login = service.login(username);
            if (login == null) {
                throw new RuntimeException("Entity not found");
            }

            Set<GrantedAuthority> auths = new HashSet<GrantedAuthority>();
            GrantedAuthority auth = new SimpleGrantedAuthority("USER");
            auths.add(auth);
            serviceUser.setClientId(login.getId());
            return new User(login.getUsername(), login.getPassword(), auths);
        }
    }
}