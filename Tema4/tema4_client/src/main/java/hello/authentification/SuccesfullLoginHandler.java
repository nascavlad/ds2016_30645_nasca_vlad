package hello.authentification;

import hello.Application;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by vnasca on 1/16/2017.
 */
@Component
public class SuccesfullLoginHandler implements AuthenticationSuccessHandler {
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Application.class.getName()+"."+AuthenticationSuccessHandler.class.getName());

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user.getUsername().equals("admin")){
            httpServletResponse.sendRedirect("/admin/admin.html");
        } else {
            httpServletResponse.sendRedirect("/user/user.html");
        }
        logger.info("Successfull login.");
    }
}
