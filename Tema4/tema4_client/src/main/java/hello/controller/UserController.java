package hello.controller;

import hello.service.ServiceAdmin;
import hello.service.ServiceUser;
import hello.wsdl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vnasca on 1/12/2017.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private ServiceUser serviceUser;

    @Autowired
    private ServiceAdmin serviceAdmin;

    @RequestMapping(method = RequestMethod.GET, path = "/getAllPackages")
    public List<PackagePojo> getAllPackages(HttpServletRequest request, final HttpServletResponse response) {
        List<PackagePojo> pojos = new ArrayList<>();
        List<PackageResponse> resp = serviceUser.getAllPackages().getPackageResponse();
        for(PackageResponse p: resp){
            pojos.add(p.getPackagePojo());
        }
        return pojos;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getPackageById/{id}")
    public PackagePojo getPackageById(@PathVariable("id") long id, HttpServletRequest request, final HttpServletResponse response) {
        return serviceUser.getPackageById(id).getPackagePojo();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getPackageByTracking/{tracking}")
    public List<PackagePojo> getPackageByTracking(@PathVariable("tracking") boolean tracking, HttpServletRequest request, final HttpServletResponse response) {
        List<PackagePojo> pojos = new ArrayList<>();
        List<PackageResponse> resp = serviceUser.getPackageByTracking(tracking).getPackageResponse();
        for(PackageResponse p: resp){
            pojos.add(p.getPackagePojo());
        }
        return pojos;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getPackageStatus/{id}")
    public StatusPojo getPackageStatus(@PathVariable("id") long id, HttpServletRequest request, final HttpServletResponse response) {
        return serviceUser.getPackageStatus(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getAllRoutes/{id}")
    public List<RoutePojo> getAllRoutes(@PathVariable("id") long id, HttpServletRequest request, final HttpServletResponse response) {
        List<RoutePojo> pojos = new ArrayList<>();
        List<RouteResponse> resp = serviceUser.getAllRoutes(id).getRouteResponse();
        for(RouteResponse p: resp){
            pojos.add(p.getRoutePojo());
        }
        return pojos;
    }
}
