package hello.controller;

import hello.service.ServiceAdmin;
import hello.wsdl.PackagePojo;
import hello.wsdl.RegisterRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by vnasca on 1/17/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/")
public class RegisterController {

    @Autowired
    private ServiceAdmin service;

    @RequestMapping(method = RequestMethod.POST, path = "/reg")
    public String register(@RequestBody RegisterRequest registerRequest, HttpServletRequest request, final HttpServletResponse response) throws IOException {
        service.register(registerRequest.getUsername(), registerRequest.getPassword());
        response.sendRedirect("/");
        return "";
    }
}
