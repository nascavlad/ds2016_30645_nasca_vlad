package hello.controller;

import hello.service.ServiceAdmin;
import hello.service.ServiceUser;
import hello.wsdl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vnasca on 1/11/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private ServiceAdmin serviceAdmin;

    @RequestMapping(method = RequestMethod.GET, path = "/getAllPackages")
    public List<PackagePojo> getAllPackages(HttpServletRequest request, final HttpServletResponse response) {
        List<PackagePojo> pojos = new ArrayList<>();
        List<PackageResponse> resp = serviceAdmin.getAllPackages().getPackageResponse();
        for(PackageResponse p: resp){
            pojos.add(p.getPackagePojo());
        }
        return pojos;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getPackageById/{id}")
    public PackagePojo getPackageById(@PathVariable("id") long id, HttpServletRequest request, final HttpServletResponse response) {
        return serviceAdmin.getPackageById(id).getPackagePojo();
    }

    @RequestMapping(method = RequestMethod.POST, path = "/createPackage")
    public PackagePojo createPackage(@RequestBody PackagePojo pack, HttpServletRequest request, final HttpServletResponse response) {
        return serviceAdmin.createPackage(pack).getPackagePojo();
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/deletePackage/{id}")
    public PackagePojo deletePackage(@PathVariable("id") long id, HttpServletRequest request, final HttpServletResponse response) {
        return serviceAdmin.deletePackage(id).getPackagePojo();
    }

    @RequestMapping(method = RequestMethod.POST, path = "/createRoute")
    public RoutePojo createRoute(@RequestBody RoutePojo route, HttpServletRequest request, final HttpServletResponse response) {
        return serviceAdmin.createRoute(route).getRoutePojo();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getAllRoutes/{id}")
    public List<RoutePojo> getAllRoutes(@PathVariable("id") long id, HttpServletRequest request, final HttpServletResponse response) {
        List<RoutePojo> pojos = new ArrayList<>();
        List<RouteResponse> resp = serviceAdmin.getAllRoutes(id).getRouteResponse();
        for(RouteResponse p: resp){
            pojos.add(p.getRoutePojo());
        }
        return pojos;
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/track")
    public PackagePojo track(@RequestBody TrackPojo trackPojo, HttpServletRequest request, final HttpServletResponse response) {
        return serviceAdmin.track(trackPojo).getPackagePojo();
    }


}