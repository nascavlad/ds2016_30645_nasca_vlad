package rabbit.consumer;

/**
 * Created by vnasca on 12/19/2016.
 */

import com.rabbitmq.client.*;
import rabbit.Dvd;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Consumer {

    private final static String QUEUE_NAME = "hello";

    public static void main(String[] argv) throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        com.rabbitmq.client.Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                Dvd message = Dvd.fromBytes(body);
                System.out.println(" [x] Received '" + message + "'");
                writeFile(message);
                sendMail(message);
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }

    private static void sendMail(Dvd dvd) {
        List<String> emails = new ArrayList<String>();
        emails.add("emailtestdstema3@gmail.com");
        emails.add("nascavlad@gmail.com");

        MailService mailService = new MailService("emailtestdstema3@gmail.com", "parolaparola");
        for (String email : emails) {
            mailService.sendMail(email, "new dvd", dvd.toString());
        }
    }

    private static void writeFile(Dvd dvd) {
        PrintWriter writer = null;
        String fileName = "dvdFile" + (int) (Math.random()*1500 % 1500) + ".txt";
        try {
            writer = new PrintWriter(fileName, "UTF-8");
            writer.println(dvd.toString());
            System.out.println("Wrote file " + fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }
}