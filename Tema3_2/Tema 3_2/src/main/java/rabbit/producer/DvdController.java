package rabbit.producer;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rabbit.Dvd;
import rabbit.producer.Producer;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by vnasca on 12/19/2016.
 */
@Controller
@CrossOrigin
@RequestMapping("/app")
public class DvdController {

    @Autowired
    Producer producer;

    @RequestMapping(path = "/{price}/{year}/{title}", method = RequestMethod.GET)
    @ResponseBody
    void calculatePrice(@PathVariable("price") String price, @PathVariable("year") String year, @PathVariable("title") String title, HttpServletRequest request, HttpServletResponse response) {
        try {
            producer.sendDvd(new Dvd(title, Integer.parseInt(year), Integer.parseInt(price)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
