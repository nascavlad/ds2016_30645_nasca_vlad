package rabbit.producer;

/**
 * Created by vnasca on 12/19/2016.
 */

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.stereotype.Component;
import rabbit.Dvd;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Component
public class Producer {

    private final static String QUEUE_NAME = "hello";

    public void sendDvd(Dvd dvd) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.basicPublish("", QUEUE_NAME, null, dvd.getBytes());
        System.out.println(" [x] Sent '" + dvd + "'");

        channel.close();
        connection.close();
    }
}