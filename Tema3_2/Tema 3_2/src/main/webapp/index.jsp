<html>
<head>
    <link rel="stylesheet" type="text/css" href="../home.css">
</head>
<body>
<div ng-app="app" ng-controller="appCtrl">

    <div class="labels">
        <label name="Year">Year</label><br>
        <label name="Price">Price</label><br>
        <label name="title">Title</label><br>
    </div>

    <div class="values">
        <input type="text" ng-model="year"/><br>
        <input type="text" ng-model="price"/><br>
        <input type="text" ng-model="title"/><br>
    </div>

    <button ng-click="callAdd()">Add</button>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="../home.js"></script>
</body>
</html>
