angular.module("app", [])
    .controller("appCtrl", function ($scope, $http) {
        var path = "http://localhost:8080/";
        $scope.flightsPretty = [];
        $scope.arrivalbool = false;
        $scope.arrival = [];
        init();


        function init() {
            $scope.flights = [];
            $scope.cities = [];


            $http.get(path + 'api/flights')
                .then(function (response) {
                    $scope.flights = response.data;
                    getCities();
                });

        }

        function getCities() {
            $http.get(path + 'api/cities')
                .then(function (response) {
                    $scope.cities = response.data;
                    var i = 0;
                    var l = $scope.flights.length;

                    for (i; i < l; i++) {
                        var flight = [];
                        flight.id = $scope.flights[i].id;
                        flight.airplaneType = $scope.flights[i].airplaneType;
                        flight.departCity = getCity($scope.flights[i].departCity).name;
                        flight.departDate = $scope.flights[i].departDate;
                        flight.arrivalCity = getCity($scope.flights[i].arrivalCity).name;
                        flight.arrivalDate = $scope.flights[i].arrivalDate;
                        $scope.flightsPretty[i] = flight;
                    }
                });
        }

        function getCity(id) {
            var i = 0;
            var l = $scope.cities.length;
            for (i; i < l; i++) {
                debugger;
                if ($scope.cities[i].id == id) {
                    return $scope.cities[i];
                }
            }
            return null;
        }

        function getCityByName(name) {
            var i = 0;
            var l = $scope.cities.length;
            for (i; i < l; i++) {
                debugger;
                if ($scope.cities[i].name == name) {
                    return $scope.cities[i];
                }
            }
            return null;
        }

        $scope.time = function(flight){
            $scope.arrivalbool = true;
            var req = {
                method: 'POST',
                url: '/api/cities',
                headers: {
                    'Content-Type': undefined
                },
                params: {
                    long: getCityByName(flight.arrivalCity).longitude,
                    lat: getCityByName(flight.arrivalCity).latitude,
                    date: flight.arrivalDate
                }
            }

            $http(req).then(function (response) {
                $scope.arrival = response.data;
            }, function (response) {
            });
        }

        $scope.logout = function(){
            $http.get(path + 'login')
                .then(function (response) {
                    window.location = "http://localhost:8080/";
                });
        }

    });