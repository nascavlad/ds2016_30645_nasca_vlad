angular.module("app", [])
.controller("appCtrl", function ($scope, $http) {
    var path = "http://localhost:8080/";
    $scope.flightsPretty = [];
    $scope.editEnable = false;
    $scope.updateOrInsert = false;
    $scope.flightToEdit = [];
    $scope.flightToDelete = [];
    init();


    function init() {
        $scope.flights = [];
        $scope.cities = [];
        debugger;

        $http.get(path + 'api/flights')
            .then(function (response) {
                $scope.flights = response.data;
                debugger;
                getCities();
        });
        debugger;
    }

    function getCities() {
        $http.get(path + 'api/cities')
            .then(function (response) {
                $scope.cities = response.data;
                debugger;
                var i = 0;
                var l = $scope.flights.length;
                $scope.flightsPretty = [];
                for (i; i < l; i++) {
                    var flight = [];
                    flight.id = $scope.flights[i].id;
                    flight.airplaneType = $scope.flights[i].airplaneType;
                    flight.departCity = getCityById($scope.flights[i].departCity);
                    flight.departDate = $scope.flights[i].departDate;
                    flight.arrivalCity = getCityById($scope.flights[i].arrivalCity);
                    flight.arrivalDate = $scope.flights[i].arrivalDate;

                    $scope.flightsPretty[i] = flight;
                }
                debugger;
            });
    }

    function getCityById(id) {
        var i = 0;
        var l = $scope.cities.length;
        for (i; i < l; i++) {
            if ($scope.cities[i].id == id) {
                return $scope.cities[i].name;
            }
        }
        return null;
    }

    function getCityByName(name) {
        var i = 0;
        var l = $scope.cities.length;
        for (i; i < l; i++) {
            if ($scope.cities[i].name == name) {
                return $scope.cities[i].id;
            }
        }
        return null;
    }
    
    $scope.editFlight = function(flight){
        $scope.editEnable = true;
        $scope.updateOrInsert = true;

        $scope.flightToEdit.id = flight.id;
        $scope.flightToEdit.airplaneType = flight.airplaneType;
        $scope.flightToEdit.departCity = flight.departCity;
        $scope.flightToEdit.departDate = flight.departDate;
        $scope.flightToEdit.arrivalCity = flight.arrivalCity;
        $scope.flightToEdit.arrivalDate = flight.arrivalDate;
    }

    $scope.addFlight = function(){
        $scope.editEnable = true;
        $scope.updateOrInsert = false;
        $scope.flightToEdit = [];
    }

    $scope.submit = function(){
        debugger;
        $scope.flightToEdit.departCity = getCityByName($scope.flightToEdit.departCity);
        $scope.flightToEdit.arrivalCity = getCityByName($scope.flightToEdit.arrivalCity);  
        debugger;
        if($scope.updateOrInsert){
            var req = {
                method: 'PUT',
                url: '/api/flights',
                headers: {
                    'Content-Type': undefined
                },
                params: { 
                    id: $scope.flightToEdit.id,
                    type: $scope.flightToEdit.airplaneType,
                    departCity: $scope.flightToEdit.departCity,
                    departDate: $scope.flightToEdit.departDate,
                    arrivalCity: $scope.flightToEdit.arrivalCity,
                    arrivalDate: $scope.flightToEdit.arrivalDate
                }
            }

            $http(req).then(function (response) {
                $scope.cancel();
                init();
            }, function (response) {
            });
        }else{
            var req = {
                method: 'POST',
                url: '/api/flights',
                headers: {
                    'Content-Type': undefined
                },
                params: { 
                    type: $scope.flightToEdit.airplaneType,
                    departCity: $scope.flightToEdit.departCity,
                    departDate: $scope.flightToEdit.departDate,
                    arrivalCity: $scope.flightToEdit.arrivalCity,
                    arrivalDate: $scope.flightToEdit.arrivalDate
                }
            }

            $http(req).then(function (response) {
                $scope.cancel();
                init();
            }, function (response) {
            });
        }
        

        
    }
    $scope.cancel = function(){
        $scope.editEnable = false;
        $scope.flightToEdit = [];
    }

    $scope.deleteFlight = function(flight) {
        $scope.flightToDelete = flight;
        debugger;
        var req = {
            method: 'DELETE',
            url: '/api/flights',
            headers: {
                'Content-Type': undefined
            },
            params: {
                id: $scope.flightToDelete.id
            }
        }

        $http(req).then(function (response) {
            debugger;
            init();
        }, function (response) {
            debugger;
        });
    }
    $scope.logout = function(){
        $http.get(path + 'login')
            .then(function (response) {
                window.location = "http://localhost:8080/";
            });
    }
});