package model;

import java.sql.Date;

public class Flight {
	private int id;
	private String airplaneType;
	private int departCity;
	private String departDate;
	private int arrivalCity;
        private String arrivalDate;
	
	public Flight(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public int getDepartCity() {
		return departCity;
	}

	public void setDepartCity(int departCity) {
		this.departCity = departCity;
	}

	public String getDepartDate() {
		return departDate;
	}

	public void setDepartDate(String departDate) {
		this.departDate = departDate;
	}

	public int getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(int arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
	
        
	
}
