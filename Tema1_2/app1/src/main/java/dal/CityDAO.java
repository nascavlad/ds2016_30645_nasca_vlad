package dal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import model.City;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * @author vnasca
 */
public class CityDAO {

    public ArrayList<City> getCities() {
        SessionFactory factory;
        factory = new Configuration().configure().buildSessionFactory();

        Session session = factory.openSession();
        Transaction tx = null;
        ArrayList<City> cities = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City");
            cities = (ArrayList<City>) query.list();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return null;
        } finally {
            session.close();
        }
        if (cities.size() < 1) {
            return null;
        } else {
            return cities;
        }
    }

    public String getTime(String lon, String lat, String date) {

        String[] s1 = date.split(" ");
        String[] s2 = s1[1].split(":");
        int time = Integer.parseInt(s2[0]);

        String diff = "";

        try {
            URL urldemo = new URL("http://www.new.earthtools.org/timezone/" + lon + "/" + lat);
            URLConnection yc = urldemo.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println(inputLine);
                if (inputLine.contains("<offset>")) {
                    diff = inputLine;
                }
            }
            in.close();

            diff = diff.replace("<offset>", "").replace("</offset>", "").replaceAll("\\s", "");
            time = time - 2 + Integer.parseInt(diff);

            if (time > 24) {
                time -= 24;
            } else if (time < 0) {
                time += 24;
            }
            return time + ":" + s2[1];

        } catch (Exception e) {
            System.out.println(e);
        }


        return null;
    }

}
