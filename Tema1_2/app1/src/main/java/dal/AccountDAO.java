package dal;

import java.awt.List;
import java.util.ArrayList;

import model.Account;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;

public class AccountDAO {

    public AccountDAO() {

    }

    public Integer addAccount(Account acc) {
        SessionFactory factory;
        factory = new Configuration().configure().buildSessionFactory();

        Session session = factory.openSession();
        Transaction tx = null;
        Integer employeeID = null;
        try {
            tx = session.beginTransaction();
            Account account = new Account();
            account.setId(acc.getId());
            account.setName(acc.getName());
            account.setPassword(acc.getPassword());
            account.setType(acc.getType());
            employeeID = (Integer) session.save(account);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return employeeID;
    }

    public Account getAccount(String name) {
        SessionFactory factory;
        factory = new Configuration().configure().buildSessionFactory();

        Session session = factory.openSession();
        Transaction tx = null;
        ArrayList<Account> accounts = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Account WHERE name = :name");
            query.setParameter("name", name);
            accounts = (ArrayList<Account>) query.list();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return null;
        } finally {
            session.close();
        }
        if (accounts.size() < 1) {
            return null;
        } else {
            return accounts.get(0);
        }
    }
}
