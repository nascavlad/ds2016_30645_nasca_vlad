package dal;

import java.util.ArrayList;

import model.Account;
import model.Flight;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class FlightDAO {

    public FlightDAO() {

    }

    public String addFlight(Flight flight) {
        SessionFactory factory;
        factory = new Configuration().configure().buildSessionFactory();

        Session session = factory.openSession();
        Transaction tx = null;
        String flightId = null;
        try {
            tx = session.beginTransaction();
            Flight f = new Flight();
            f.setAirplaneType(flight.getAirplaneType());
            f.setDepartCity(flight.getDepartCity());
            f.setDepartDate(flight.getDepartDate());
            f.setArrivalCity(flight.getArrivalCity());
            f.setArrivalDate(flight.getArrivalDate());
            
            flightId = "da";
            session.save(f);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flightId;
    }

    public Flight getFlight(int id) {
        SessionFactory factory;
        factory = new Configuration().configure().buildSessionFactory();

        Session session = factory.openSession();
        Transaction tx = null;
        ArrayList<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flights = (ArrayList<Flight>) query.list();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return null;
        } finally {
            session.close();
        }
        if (flights.size() < 1) {
            return null;
        } else {
            return flights.get(0);
        }
    }

    public ArrayList<Flight> getFlights() {
        SessionFactory factory;
        factory = new Configuration().configure().buildSessionFactory();

        Session session = factory.openSession();
        Transaction tx = null;
        ArrayList<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight");
            flights = (ArrayList<Flight>) query.list();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return null;
        } finally {
            session.close();
        }
        if (flights.size() < 1) {
            return null;
        } else {
            return flights;
        }
    }

    public boolean deleteFlight(int id) {
        SessionFactory factory;
        factory = new Configuration().configure().buildSessionFactory();

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("DELETE FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return false;
        } finally {
            session.close();
        }
        return true;
    }
    
    public boolean updateFlight(Flight flight) {
        SessionFactory factory;
        factory = new Configuration().configure().buildSessionFactory();

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("UPDATE Flight set airplane_type = :a , depart_city = :b , depart_date = :c , arrival_city = :d , arrival_date = :e WHERE id = :id");
            query.setParameter("a", flight.getAirplaneType());
            query.setParameter("b", flight.getDepartCity());
            query.setParameter("c", flight.getDepartDate());
            query.setParameter("d", flight.getArrivalCity());
            query.setParameter("e", flight.getArrivalDate());
            query.setParameter("id", flight.getId());
            query.executeUpdate();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return false;
        } finally {
            session.close();
        }
        return true;
    }
}
