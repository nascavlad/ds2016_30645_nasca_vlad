package filters;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import servlet.FirstServlet;

import java.util.*;
import java.util.logging.Logger;

public class LoginFilter implements Filter {
	
	private static final Logger LOGGER = Logger.getLogger(FirstServlet.class.getName());
	
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {    
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String permission = session == null? null : (String) session.getAttribute("permission");
        boolean admin = permission == null ? false : permission.equals("admin");
        
        if (admin) {
        	LOGGER.warning("Letting user pass filter with permission " + permission);
            chain.doFilter(request, response);
        } else {
        	LOGGER.warning("Not letting someone pass admin filter");
        	response.sendRedirect("http://localhost:8080/index.jsp");
        }
    }

	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}