package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import dal.CityDAO;
import model.City;


/**
 * Servlet implementation class CityServlet
 */
public class CityServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        CityDAO dao = new CityDAO();
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<City> list = new ArrayList();
        list = dao.getCities();
        out.print(mapper.writeValueAsString(list));
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CityDAO dao = new CityDAO();

        String lon = request.getParameter("long");
        String lat = request.getParameter("lat");
        String date = request.getParameter("date");


        PrintWriter out = response.getWriter();
        ObjectMapper mapper = new ObjectMapper();
        out.print(mapper.writeValueAsString(dao.getTime(lon, lat, date)));
        out.close();
    }
}
