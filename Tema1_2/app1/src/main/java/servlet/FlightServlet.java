package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Flight;

import com.fasterxml.jackson.databind.ObjectMapper;

import dal.FlightDAO;
import java.sql.Date;

/**
 * Servlet implementation class FlightServlet
 */
public class FlightServlet extends HttpServlet {
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		FlightDAO dao = new FlightDAO();
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<Flight> list = new ArrayList();
		list = dao.getFlights();
		out.print(mapper.writeValueAsString(list));
		out.close();
	}
        
        @Override
        protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FlightDAO dao = new FlightDAO();

                Flight f = new Flight();
                f.setId(Integer.parseInt(request.getParameter("id")));
                f.setAirplaneType(request.getParameter("type"));
                f.setArrivalCity(Integer.parseInt(request.getParameter("arrivalCity")));
                f.setDepartDate(request.getParameter("departDate"));
                f.setDepartCity(Integer.parseInt(request.getParameter("departCity")));
                f.setArrivalDate(request.getParameter("arrivalDate"));
                dao.updateFlight(f);
	}
        
        @Override
        protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FlightDAO dao = new FlightDAO();
                dao.deleteFlight(Integer.parseInt(request.getParameter("id")));
	}
        
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FlightDAO dao = new FlightDAO();

                Flight f = new Flight();
                f.setAirplaneType(request.getParameter("type"));
                f.setArrivalCity(Integer.parseInt(request.getParameter("arrivalCity")));
                f.setDepartDate(request.getParameter("departDate"));
                f.setDepartCity(Integer.parseInt(request.getParameter("departCity")));
                f.setArrivalDate(request.getParameter("arrivalDate"));
                dao.addFlight(f);
	}
}
