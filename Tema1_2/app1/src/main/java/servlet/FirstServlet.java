package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Account;


import dal.AccountDAO;

public class FirstServlet extends HttpServlet{
	
	 private static final Logger LOGGER = Logger.getLogger(FirstServlet.class.getName());
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException{
		HttpSession session=request.getSession(false);
		if(session != null){
			session.invalidate();
		}
		response.sendRedirect("index.jsp");
		LOGGER.warning("---------invalidating session");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)  
	        throws  IOException {  
	  
	    response.setContentType("text/html");  
	    PrintWriter out = response.getWriter();  
	          
	    String n=request.getParameter("username");  
	    String p=request.getParameter("userpass");
	    
	    Account acc = validateAccount(n,p);

	    if(acc == null){
	    	response.sendRedirect("index.jsp");
	    }else if(acc.getType().equals("admin")){
	    	setSession(acc,request);
	    	response.sendRedirect("admin/html/admin.jsp"); 
	    }else{
	    	setSession(acc,request);
	    	response.sendRedirect("user/html/user.jsp"); 
	    }     
	    out.close();  
	}
	
	private Account validateAccount(String name, String password){
		AccountDAO dao = new AccountDAO();
		Account acc = dao.getAccount(name);
		if(acc == null){
			LOGGER.warning(" acc from dao null");
			return null;
		}
		if(!acc.getPassword().equals(password)){
			System.out.println(acc.getPassword() + "  " + password);
			LOGGER.warning("password did not match");
			return null;
		}
		LOGGER.warning("account is ok");
		return acc;
	}
	
	private void setSession(Account acc, HttpServletRequest request){
		HttpSession session=request.getSession(true);
        session.setAttribute("name",acc.getName());
        session.setAttribute("permission", acc.getType());
        LOGGER.warning("Authenticated user with permission " + acc.getType());
	}
	
}
