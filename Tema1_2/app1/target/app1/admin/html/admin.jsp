<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/user.css">
</head>
<body>
    <div  ng-app="app" ng-controller="appCtrl">
        <h2 align="center">Flights</h2>
        <button ng-click="logout()">Logout</button>
        <table border="1" class="center" >
            <thead>
                <tr>
                    <th>Airplane type</th>
                    <th>Departure city</th>
                    <th>Departure date</th> 
                    <th>Arrival City</th>
                    <th>Arrival Date</th>
                    <th width="30px"></th>
                    <th width="30px"></th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="flight in flightsPretty">
                    <td >{{flight.airplaneType}}</td>
                    <td >{{flight.departCity}}</td>
                    <td >{{flight.departDate}}</td>
                    <td >{{flight.arrivalCity}}</td>
                    <td >{{flight.arrivalDate}}</td>
                    <td>
                        <div class="edit-button"  ng-click="editFlight(flight)" type="submit" value = ""></div>
                    </td>
                    <td>
                        <div class="delete-button"  ng-click="deleteFlight(flight)" type="submit" value = ""></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="add-button"  ng-click="addFlight()" type="submit" value = ""></div>
        
        <div ng-if="editEnable == true">
            <div class="labels">
                <label name="Airplane type" >Airplane type</label><br>
                <label name="Departure City">Departure City</label><br>
                <label name="Departure Date">Departure Date</label><br>
                <label name="Arrival City">Arrival City</label><br>
                <label name="Arrival Date">Arrival Date</label><br>
            </div>

            <div class="values">
                <input type="text" ng-model="flightToEdit.airplaneType" /><br>
                <input type="text" ng-model="flightToEdit.departCity" /><br>
                <input type="text" ng-model="flightToEdit.departDate" /><br>
                <input type="text" ng-model="flightToEdit.arrivalCity" /><br>
                <input type="text" ng-model="flightToEdit.arrivalDate" /><br>
            </div>
            <button ng-click="submit()">submit</button>
            <button ng-click="cancel()">cancel</button><br>
            Date in format: dd.mm.yyyy hh:mm<br>

            <table border="1" class="dates" >
                <thead>
                    <tr>
                        <th>City</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="city in cities">
                        <td >{{city.name}}</td>
                    </tr>
                </tbody>
            </table>
        </div>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="../js/admin.js"></script> 
</body>
</html>
