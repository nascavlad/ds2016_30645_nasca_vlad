<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/user.css">
</head>
<body>
    <div  ng-app="app" ng-controller="appCtrl">
        <h2 align="center">Flights</h2>
        <button ng-click="logout()">Logout</button>
        <table border="1" class="center" >
            <thead>
                <tr>
                    <th>Airplane type</th>
                    <th>Departure city</th>
                    <th>Departure date</th>
                    <th>Arrival City</th>
                    <th>Arrival Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="flight in flightsPretty">
                    <td >{{flight.airplaneType}}</td>
                    <td >{{flight.departCity}}</td>
                    <td >{{flight.departDate}}</td>
                    <td >{{flight.arrivalCity}}</td>
                    <td >{{flight.arrivalDate}}</td>
                    <td>
                        <div class="edit-button"  ng-click="time(flight)" type="submit" value = ""></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <div ng-if="arrivalbool">Flight arrival: {{arrival}}</div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="../js/user.js"></script> 
</body>
</html>
